ARG ERPNEXT_VERSION
FROM frappe/erpnext-worker:${ERPNEXT_VERSION}

COPY . ../apps/excel_erpnext
COPY repos ../apps

USER root

RUN install-app excel_erpnext && \
    install-app whitelabel

USER frappe
