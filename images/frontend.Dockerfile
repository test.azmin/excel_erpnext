ARG FRAPPE_VERSION
ARG ERPNEXT_VERSION

FROM frappe/assets-builder:${FRAPPE_VERSION} as assets

WORKDIR /home/frappe/frappe-bench
COPY --chown=frappe:frappe repos apps

COPY . apps/excel_erpnext
COPY repos apps/whitelabel

# RUN bench setup requirements

# RUN bench build --production --verbose --hard-link

RUN install-app excel_erpnext
RUN install-app whitelabel

FROM frappe/erpnext-nginx:${ERPNEXT_VERSION}

COPY --from=assets /out /usr/share/nginx/html
